<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Register</title>
	<link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css' media="screen" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" type="text/css" />
</head>
<style>
	.container {
		display: flex;
		align-items: center;
		justify-content: center;
		height: auto;
		margin-top: 2rem;
	}

	.form-group {
		width: 30vw;
		height: inherit;
		background-color: white;
		padding: 2rem;
		margin-top: 2rem;
		display: flex;
		flex-direction: column;
		border: 2px solid #4f85b4;
	}

	.form-child {
		height: 36px;
		margin: 10px 0px;
		display: flex;
		align-items: center;
		justify-content: space-around;
		margin: 1.4rem 0px;
	}

	.form-text {
		background-color: #70ad46;
		height: inherit;
		width: 100px;
		display: flex;
		align-items: center;
		justify-content: center;
		padding: 0px 6px;
		border: 2px solid #4f85b4;
		color: white;
	}

	.sub-form-text {
		height: inherit;
		width: 200px;
		padding: 0px;
		border: 2px solid #4f85b4;
	}


	.form-radio {
		display: flex;
		align-items: center;
		height: inherit;
		width: 200px;
		padding: 0px;
	}

	.form-select {
		display: flex;
		align-items: center;
		height: inherit;
		width: 200px;
		padding: 0px
	}

	.form-birthday {
		display: flex;
		align-items: center;
		height: inherit;
		width: 160px;
		margin-right: 40px;
		padding: 0px 0px 0px 10px;
		box-sizing: border-box;
		border: 2px solid #4f85b4;
	}

	.sub-form-image {
		width: 200px;
		padding: 0px;
	}

	.form-btn {
		display: flex;
		align-items: center;
		justify-content: center;
		margin-top: 2rem;
	}

	.btn-submit {
		height: 35px;
		width: 130px;
		border-radius: 5px;
		border: 2px solid #4f85b4;
		background-color: #70ad46;
		color: white;
	}
</style>



<body>
	<?php
	// validate date format dd/MM/yyyy
	function isDate($string)
	{
		$matches = array();
		$pattern = '/^([0-9]{1,2})\\/([0-9]{1,2})\\/([0-9]{4})$/';
		if (!preg_match($pattern, $string, $matches)) return false;
		if (!checkdate($matches[2], $matches[1], $matches[3])) return false;
		return true;
	}
	// Code PHP xử lý validate
	$error = array();
	$data = array();
	if (!empty($_POST['btnSubmit'])) {
		session_start();

		date_default_timezone_set('Asia/Ho_Chi_Minh');
		$time = date('YmdHis');

		if (!file_exists("upload")) {
			mkdir("upload");
		};

		$target_dir = "upload/";

		$target_file = $target_dir . basename($_FILES["image"]["name"]);

		$nameImageArr = explode('.', $_FILES["image"]["name"]);

		$nameImageNew = "upload/" . $nameImageArr[0] . '_' . $time . '.' . $nameImageArr[1];

		$is_upload_image = true;

		$imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);

		if (file_exists($target_file)) {
			$is_upload_image = false;
		}
		if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
			$is_upload_image = false;
		}
		if ($_FILES["image"]["size"] > 100000) {
			$is_upload_image = false;
		}
		if ($is_upload_image && move_uploaded_file($_FILES["image"]["tmp_name"], $nameImageNew)) {
			$_SESSION["image"] = $nameImageNew ?? "";
		}

		// Lấy dữ liệu
		$data['fullName'] = isset($_POST['fullName']) ? $_POST['fullName'] : '';
		$data['gender'] = isset($_POST['gender']) ? $_POST['gender'] : '';
		$data['khoa'] = isset($_POST['khoa']) ? $_POST['khoa'] : '';
		$data['birthday'] = isset($_POST['birthday']) ? $_POST['birthday'] : '';
		$data['address'] = isset($_POST['address']) ? $_POST['address'] : '';
		// Kiểm tra định dạng dữ liệu
		if (empty($data['fullName'])) {
			$error['fullName'] = 'Hãy nhập tên.';
		}

		if (empty($data['gender'])) {
			$error['gender'] = 'Hãy chọn giới tính.';
		}
		if (empty($data['khoa'])) {
			$error['khoa'] = 'Hãy chọn phân khoa.';
		}
		if (empty($data['birthday'])) {
			$error['birthday'] = 'Hãy nhập ngày sinh.';
		}
		if (!isDate($data['birthday'])) {
			$error['birthdayFormat'] = 'Hãy nhập ngày sinh đúng định dạng.';
		}
		if (!$is_upload_image) {
			$error['image'] = 'Chưa tải ảnh lên';
		}
		if (!$error) {
			$_SESSION['data'] = $data;
			header("Location: ./confirm.php");
		}
	}
	?>
	<div class="container">
		<div class="form-group">
			<form action="Register.php" method="POST" id="form" enctype="multipart/form-data">
				<?php
				if ($error) {
					foreach ($error as $key => $value) {
						echo '
                <p style="color: red">' . $value . '</p>
              ';
					}
				}
				?>
				<div class="form-child">
					<label class="form-text">
						Họ và tên
						<span style="color: red">*</span>
					</label>
					<input type="text" name="fullName" id="fullName" class="sub-form-text">
				</div>

				<div class="form-child">
					<label class="form-text">
						Giới tính
						<span style="color: red">*</span>
					</label>
					<div class="form-radio">
						<?php
						$gender = array('0' => 'Nam', '1' => 'Nữ');
						for ($i = 0; $i < count($gender); $i++) {
							echo '
                <input type="radio" id="' . $i . '" name="gender" value="' . $gender[$i] . '">
                ';
							echo '
                <label for="' . $i . '" style="margin: 6px 12px 0px">' . $gender[$i] . '</label> 
                ';
						}
						?>
					</div>
				</div>

				<div class="form-child">
					<label class="form-text">
						Phân khoa
						<span style="color: red">*</span>
					</label>
					<div class="form-select">
						<select id="khoa" name="khoa" id="khoa" style="height: inherit">
							<?php
							$khoa = array('0' => '', 'MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');
							foreach ($khoa as $key => $value) {
								echo '<option value="' . $key . '">' . $value . '</option>';
							}
							?>
						</select>
					</div>
				</div>

				<div class="form-child" date-date-format="dd/MM/yyyy">
					<label class="form-text">
						Ngày sinh
						<span style="color: red">*</span>
					</label>
					<input type="text" name="birthday" id="birthday" class="form-birthday" placeholder="dd/mm/yyyy">
				</div>

				<div class="form-child">
					<label class="form-text">
						Địa chỉ
					</label>
					<input type="text" name="address" id="address" class="sub-form-text">
				</div>

				<div class="form-child">
					<label class="form-text">
						Hình ảnh
					</label>
					<input type="file" name="image" id="image" class="sub-form-image">
				</div>
				<div class="form-btn">
					<input type="submit" name="btnSubmit" id="btnSubmit" class="btn-submit" value="Đăng ký">
				</div>

			</form>
		</div>
	</div>
	<?php
	?>
	<script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
	<script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
	<!-- Bootstrap -->
	<!-- Bootstrap DatePicker -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript"></script>
	<!-- Bootstrap DatePicker -->
	<script type="text/javascript">
		$(function() {
			$('#birthday').datepicker({
				format: "dd/mm/yyyy"
			});
		});
	</script>
</body>

</html>